//定义store

import { defineStore } from "pinia"
import { useCart } from "./cart"
export const useUser = defineStore('users', {
    state() {
        return {
            username: '卢本伟',
            count: 10,
            arr: ['吃', '喝', '看球'],
            data: []
        }
    },
    getters: {
        reverseName(state) {
            return state.username.split("").reverse().join("")
        },
        newArr(state) {
            // 将翻转的名字,push到arr这个状态中
            state.arr.push(this.reverseName)

            return state.arr;
        },
        // 给getters传递参数
        newCount(state) {
            //从计算属性的内部返回一个函数,来支持参数的传递


            //这么写会失去缓存的效果
            return (c) => {
                return state.count + c
            }
        }
    },
    actions: {
        async fetch_data(a, b) {
            // console.log(a, b)
            //获取另一个模块的store
            let cartStore = useCart()

            console.log('--cartStore', cartStore.cart)
            try {
                let res = await fetch('/api/data/index.json')
                    .then(body => body.json())
                console.log(res)
                // 直接修改状态
                this.data = res.data
            } catch (e) {
                console.log(e)
            }

        }
    }
})