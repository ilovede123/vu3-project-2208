import { createApp } from 'vue'

import App from './App.vue'
import router from "./router"
import bus from "./utils/bus"

//引入全局的less文件
import "./assets/global.less"
//引入Pinia
import { createPinia } from "pinia"
//引入elementui
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

let app = createApp(App)

//将bus添加到全局
app.config.globalProperties.$bus = bus;

app.use(router)

app.use(createPinia())
app.use(ElementPlus)

app.mount('#app')
