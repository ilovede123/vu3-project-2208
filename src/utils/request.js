//引入axios

import axios from "axios"

//配置axios

let http = axios.create({
    //基础地址
    baseURL: '/api',
    // 超时设置
    timeout: 1000 * 10,
    //是否携带凭据
    withCredentials: true
})

//配置拦截器

//请求拦截器,所有的axios请求都会被拦截器拦截,可以通过拦截器添加额外的配置

http.interceptors.request.use(config => {
    return config
})

//响应拦截器,所有的axios响应都会被响应拦截器拦截,可以对拦截的响应进行额外的配置

http.interceptors.response.use(config => {
    return config
})

export default http;