import { reactive } from "vue";
//校验函数
//校验用户名
export const validateUsername = (rule, value, callback) => {
    //英文字母,数字,下划线或者短横线3-16位
    let reg = /^[a-zA-Z0-9-_]{3,16}$/
    console.log(value)
    if (reg.test(value)) {
        //验证通过
        callback()
    } else {
        //验证不通过
        callback(new Error('用户名必须由英文字母,数字,下划线或者短横线3-16位'))
    }
}
//校验password
export const validatePassword = (rule, value, callback) => {
    if (value) {
        callback()
    } else {
        callback(new Error('密码不能为空'))
    }
}
//校验验证码
export const validateCaptcha = (rule, value, callback) => {
    if (value.length < 5) {
        callback(new Error('验证码不能小于5位'))
    } else {
        callback()
    }
}

//校验手机号

export const validatePhone = (rule, value, callback) => {
    //手机号正则
    let reg = /^1(3\d|4[5-9]|5[0-35-9]|6[567]|7[0-8]|8\d|9[0-35-9])\d{8}$/;
    if (reg.test(value)) {
        callback()
    } else {
        callback(new Error('手机号格式不正确'))
    }
}

//校验手机验证码
export const validateCode = (rule, value, callback) => {
    if (value.length < 5) {
        callback(new Error('验证码最少5位'))
    } else {
        callback()
    }
}

//校验规则
//校验常规登入的验证规则
export const commonRules = reactive({
    username: [{ validator: validateUsername, trigger: 'blur' }],
    password: [{ validator: validatePassword, trigger: 'blur' }],
    captcha: [{ validator: validateCaptcha, trigger: 'blur' }],
})

//校验手机登入的验证规则

export const smsRules = reactive({
    phone: [{ validator: validatePhone, trigger: 'blur' }],
    code: [{ validator: validateCode, trigger: 'blur' }],
})

//表单提交的时候
/**
 * 
 * @param {Object} formEl 表单的引用对象
 * @param {Function} successCallback 表单校验通过的回调函数
 * @param {Function} faildCallback  表单校验失败的回调函数
 */
export const useSubmitForm = (successCallback, faildCallback) => (formEl) => {
    // console.log(formEl)
    if (!formEl) return
    //调用了组件提供的校验的方法
    formEl.validate((valid) => {
        // 如果所有的表单校验通过,valid就是true,不然就是false
        if (valid) {
            //登入请求可以在这里发送
            if (successCallback) {
                successCallback()
            }
            // console.log('submit!')
        } else {
            // console.log('error submit!')
            if (faildCallback) {
                faildCallback(false)
            }
            return false
        }
    })
} 