import { createRouter, createWebHashHistory, createWebHistory, createMemoryHistory } from "vue-router"


//1.声明routers配置,一个routes是一个数组,routes对象里面的基本属性由path和component组成


let routes = [
    {
        name: "login",
        path: "/login",
        component: () => import(/*webpackChunkName:'login' */"../pages/login/index.vue")
    },
    {
        name: 'home',
        path: '/',
        component: () => import("../pages/Home/index.vue")
    }
]

//2.创建路由
const router = createRouter({
    history: createWebHashHistory(""),
    routes
})


//3.导出路由
export default router