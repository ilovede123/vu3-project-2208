import http from "../utils/request"

//获取验证码

export let getCaptchaApi = () => http.get("/users/getCaptcha?v=next")

//校验验证码

export let verifyCaptchaApi = captcha => http.get("/users/verifyCaptcha?captcha=" + captcha)

//用户名密码登入

export let loginApi = (username, password) => http.post("/users/login", {
    username,
    password
})

//获取手机验证码

export let getSmsCode = phoneNumber => http.post('/sms/send', { phoneNumber })

//手机验证码登入

export let smsApi = code => http.post('/users/login?type=sms', { code })