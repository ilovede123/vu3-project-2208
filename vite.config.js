import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
//node中的path模块
import path from "path"
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  base: "./",//配置打包之后的静态资源目录
  resolve: {
    // 别名配置
    alias: {
      "@": path.resolve('src'),
      "component": path.resolve('src', 'component'),
      "pages": path.resolve('src', 'pages')
    }
  },
  server: {
    port: 8080,
    proxy: {
      "/api": {
        target: "http://chst.vip",
        rewrite: (path) => path.replace(/^\/api/, "")
      }
    }
  }
})
